import logging
name = 'AixCIlenz CI Driver'
version = '0.5.4'

debug = False
stdout_logging = False
log_level = logging.DEBUG

seconds_per_day = 86400
cc_tmp_del_range = 2
