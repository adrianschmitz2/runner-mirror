def add_parameters(parser, param_list):
    for p in param_list:
        if p.get("action"):
            parser.add_argument(
                *p.get("flags"),
                required=p.get("required"),
                default=p.get("default"),
                action=p.get("action"),
                help=p.get("help")
            )
        else:
            parser.add_argument(
                *p.get("flags"),
                required=p.get("required"),
                type=p.get("type"),
                choices=p.get("choices"),
                nargs=p.get("nargs"),
                default=p.get("default"),
                const=p.get("const"),
                metavar=p.get("metavar"),
                help=p.get("help")
            )
