import json

import rsa
import os
import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES


def load_priv_key(path):
    with open(path, mode='rb') as private_file:
        key_data = private_file.read()
    return rsa.PrivateKey.load_pkcs1(key_data)


def load_pub_key(path):
    with open(path, mode='rb') as public_file:
        key_data = public_file.read()
    return rsa.PublicKey.load_pkcs1(key_data)


def create_keys(private_key_path, pub_key_path):
    (pubkey, private_key) = rsa.newkeys(2048)
    with open(f"{pub_key_path}", "w") as text_file:
        text_file.write(pubkey.save_pkcs1().decode('ascii'))
    with open(f"{private_key_path}", "w") as text_file:
        text_file.write(private_key.save_pkcs1().decode('ascii'))


def read_mapping(key_path, map_path, AES_key):
    key = get_AES_key(AES_key, key_path)
    cipher = AESCipher(key)
    with open(map_path, mode='rb') as file:
        file.seek(0)
        data = file.read()
    try:
        json_file = cipher.decrypt(data)
        search_file = json.loads(json_file)
        return search_file
    except ValueError:
        return {}


def write_mapping(mapping, key_path, map_path, AES_key):
    key = get_AES_key(AES_key, key_path)
    cipher = AESCipher(key)
    json_file = json.dumps(mapping)
    encrypted = cipher.encrypt(json_file)
    with open(map_path, "wb") as text_file:
        text_file.write(encrypted)


def get_AES_key(file_path: str, key_path: str) -> str:
    with open(file_path, mode='rb') as file:
        file.seek(0)
        data = file.read()
    key = rsa.decrypt(data, load_priv_key(key_path))
    return key.decode('ascii')


def set_AES_key(key_phrase: str, file_path: str, key_path: str) -> None:
    encrypted = rsa.encrypt(key_phrase.encode('ascii'), load_pub_key(key_path))
    with open(file_path, "wb") as text_file:
        text_file.write(encrypted)


class AESCipher(object):

    def __init__(self, key):
        self.bs = AES.block_size
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw.encode()))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]
