from core.modes.common import *
from .srun import Slurm
from .batch import Default, Sbatch
from core.modes.base import ModeBase


class Singularity(Slurm, ABC):
    container = ''

    def get_run_parameters(self):
        parameters = Slurm.get_run_parameters(self)
        # get container for singularity
        self.container = get_cenv('CONTAINER')
        if self.container is None:
            ModeBase.abort(self, "Error: No container defined, use variable CONTAINER")
        if os.path.exists(self.container):
            self.container = f'{self.job.clone_path}/{self.container}'
        # add singularity specific parameter
        return parameters + f' --export=CONTAINER={self.container}'

    def get_run_wrapper(self):
        # Generation of the singularity script within user space
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         wrapper_add=f'/usr/bin/cp /dev/stdin '
                                                     f'{self.job.user_path}/script{self.job.jobid}.sh',
                                         script=self.get_singularity_script())
        return Slurm.get_run_wrapper(self) + f" {self.job.user_path}/script{self.job.jobid}.sh"

    def cleanup(self):
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                         wrapper_add=f'/usr/bin/rm {self.job.user_path}/wrapper{self.job.jobid}.sh')
        ModeBase.cleanup(self)
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                         wrapper_add=f'/usr/bin/rm {self.job.user_path}/script{self.job.jobid}.sh')
        self.executor.cancel(f'{ModeBase.get_jobid_from_file(self.slurm_jobid_file)}')

    def get_singularity_script(self):
        script = ""
        # get container for singularity
        self.container = get_cenv('CONTAINER')
        if self.container is None:
            ModeBase.abort(self, "Error: No container defined, use variable CONTAINER")
        if os.path.exists(self.container):
            script += f'{self.job.scripts_path}/singularityLocalRunstep.sh'
        else:
            script += f'{self.job.scripts_path}/singularityRunstep.sh'
        return script


class Singularity_Batch(Default, ABC):
    container = ''

    @staticmethod
    def escape(s):
        return s.replace('/', '\/')

    def custom_run_setup(self, **kwargs):
        Sbatch.custom_run_setup(self, **kwargs)
        if kwargs["main_script"] and kwargs["run_async"]:
            logging.debug('Creating param file')
            # write env_file
            with open(f'{self.job.tmp_dir}/env{self.job.jobid}', 'w') as file:
                file.write(f"CONTAINER={self.container}\0EXEC_WRAPPER={srun_path}\0PARAMS={self.job.get_parameters()}"
                           f"\0OUTPUT_FILE=--output={self.slurm_output_file}")
            self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                             wrapper_add=f'/usr/bin/cp /dev/stdin '
                                                         f'{self.slurm_output_dir}/batchEnv{self.job.jobid}',
                                             script=f"{self.job.tmp_dir}/env{self.job.jobid}")

    def get_run_script(self):
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         wrapper_add=f'/usr/bin/cp /dev/stdin '
                                                     f'{self.job.clone_path}/script{self.job.jobid}.sh',
                                         script=f'{Slurm.get_run_script(self)}')
        # Generation of the singularity script within user space
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         wrapper_add=f'/usr/bin/cp /dev/stdin '
                                                     f'{self.job.clone_path}/singularity{self.job.jobid}.sh',
                                         script=self.get_singularity_script())
        # move wrapper to user
        tmp = os.getenv("TMP")
        with open(f'{self.job.runner_path}/utility/runtime/scripts/batchWrapper.sh', 'r') as file:
            filedata = file.read()
        filedata = filedata.replace('replacescript',
                                    f'{self.job.clone_path}/singularity{self.job.jobid}.sh < '
                                    f'{self.job.clone_path}/script{self.job.jobid}.sh')
        with open(f'{tmp}/wrapper{self.job.jobid}', 'w') as file:
            file.write(filedata)
        return f'{tmp}/wrapper{self.job.jobid}'

    def get_run_parameters(self):
        parameters = Default.get_run_parameters(self)
        # get container for singularity
        self.container = get_cenv('CONTAINER')
        if self.container is None:
            ModeBase.abort(self, "Error: No container defined, use variable CONTAINER")
        if os.path.exists(self.container):
            self.container = f'{self.job.clone_path}/{self.container}'
        return parameters + f' --export-file={self.slurm_output_dir}/batchEnv{self.job.jobid}'

    def cleanup(self):
        ModeBase.cleanup(self)
        self.executor.cancel(f'{ModeBase.get_jobid_from_file(self.slurm_jobid_file)}')

    def get_singularity_script(self):
        script = ""
        # get container for singularity
        self.container = get_cenv('CONTAINER')
        if self.container is None:
            ModeBase.abort(self, "Error: No container defined, use variable CONTAINER")
        if os.path.exists(self.container):
            script += f'{self.job.scripts_path}/singularityLocalRunstep.sh'
        else:
            script += f'{self.job.scripts_path}/singularityRunstep.sh'
        return script
