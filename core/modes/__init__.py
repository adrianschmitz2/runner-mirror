from abc import ABC, abstractmethod
import os
import importlib
import logging

from core.utility import get_cenv
import core.utility as utility

from core.modes.ssh import SSH
from core.modes.slurm import (
    Slurm,
    Sbatch,
    Batch,
    Default,
    SingleSlurmJobAcrossStages,
    Singularity,
    Singularity_Batch,
)

VALID_MODES = ['Default', 'Slurm', 'Singularity', 'Batch', 'SingleSlurmJobAcrossStages', 'Sbatch']
DOWN_MODES = ['Singularity_Batch']
XLOCAL_MODES = ['SSH']
ALL_MODES = VALID_MODES + XLOCAL_MODES + DOWN_MODES


# Get the possible modes from the CI script
def get_mode(job):
    mode = get_cenv('CI_MODE', 'Default').strip()
    if mode not in ALL_MODES:
        print(f'Error: tried to use unknown ci mode {mode}, valid modes are {ALL_MODES}')
        logging.error(f'Error: tried to use unknown ci mode {mode}')
        exit(1)
    if job.down_scoping and mode in XLOCAL_MODES:
        print(f'Error: tried to use local only ci mode {mode}, valid modes for down_scoping are {VALID_MODES + DOWN_MODES}')
        logging.error(f'Error: tried to use local only ci mode {mode}')
        exit(1)
    if not job.down_scoping and mode in DOWN_MODES:
        print(f'Error: tried to use downscope only ci mode {mode}, valid modes for local are {VALID_MODES + XLOCAL_MODES}')
        logging.error(f'Error: tried to use downscope only ci mode {mode}')
        exit(1)
    return getattr(importlib.import_module('core.modes'), mode)(job)
