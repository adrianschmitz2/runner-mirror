from abc import ABC, abstractmethod
import os
import subprocess
import re
import importlib
import sys
from filelock import FileLock
import json
import core.authentication.JSONManager as man
import logging
import time

from core.utility import get_cenv
import core.utility as utility

srun_path = "srun"  # "/usr/local_host/bin/srun"
sbatch_path = "sbatch"  # "/usr/local_host/bin/sbatch"
