from core.modes.common import *
from core.modes.base import ModeBase
from core.utility.executor import SshExecutor


class SSH(ModeBase):
    def __init__(self, job):
        ModeBase.__init__(self, job)
        self.dest_node = get_cenv('CI_SSH_HOST')
        if not self.dest_node:
            ModeBase.abort(self, "Using ssh mode but no node specified. Specify: CI_SSH_HOST")
        self.executor = SshExecutor(job, job.down_scoping)

    def get_env_setup(self):
        return f' {self.job.driver_path}/core/scripts/ssh.env '

    def get_run_properties(self):
        return f'ssh -T {self.dest_node}'

    # TODO: Move this to ModeBase (is same for Slurm, except Batch)
    def get_run_script(self):
        self._run_script = f'{self.job.exec_script}'
        return self._run_script

    def get_combiner_script(self):
        self._combiner_script = f"{self.job.driver_path}/core/scripts/xPipeHelper.sh"
        return self._combiner_script

    def run_simple_script(self):
        out = self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         params=f'-T {self.dest_node}',
                                         script=self.get_simple_run_script())

    def run_main_script(self):
        out = self.executor.run_direct(params=f'-T {self.dest_node}', script=self.get_run_script())
        print(out)


    def cleanup(self):
        ModeBase.cleanup(self)

    def get_custom_prepare(self):
        pass

    @staticmethod
    def has_post_run_script():
        return False
