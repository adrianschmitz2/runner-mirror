from abc import ABC, abstractmethod
import os
import subprocess
import re
import importlib
import sys
from filelock import FileLock
import json
import core.authentication.JSONManager as man
import logging

from core.utility import get_cenv
import core.utility as utility
from core.utility.executor import Executor


class ModeBase(ABC):
    _custom_env = dict()
    slurm_jobid_file = None
    executor = None

    @staticmethod
    def get_jobid_from_file(path):
        with open(path, 'r') as node_index_fp:
            return node_index_fp.readline().strip()

    @abstractmethod
    def get_combiner_script(self):
        pass

    @property
    def custom_env(self):
        return self._custom_env

    def __init__(self, job):
        self.job = job
        self.executor = Executor.__init__(job, job.down_scoping)

    def get_run_parameters(self):
        return ''

    def get_run_wrapper(self):
        return ''

    def get_run_script(self):
        return ''

    def get_simple_run_script(self):
        return self.job.args[2]

    def get_simple_run_parameters(self):
        return ''

    def get_simple_run_wrapper(self):
        return ''

    def run_simple_script(self):
        pass

    def run_main_script(self):
        pass

    def run_post_script(self):
        pass

    def get_post_run_script(self):
        return None

    def custom_run_setup(self, **kwargs):
        return ''

    def get_post_run_parameters(self):
        return ''

    def get_post_run_wrapper(self):
        return ''

    def custom_prepare(self):
        pass

    def get_simple_script_exec(self):
        return None

    def inbetween_processing(self):
        pass

    @staticmethod
    def get_env_setup():
        return ''

    def get_custom_config(self):
        return None

    def cleanup_on_failure(self):
        if not self.slurm_jobid_file or not os.path.isfile(self.slurm_jobid_file):
            logging.debug(f'Skipping cleanup_on_failure due to missing slurm jobid file')
            return
        command = []
        if self.job.down_scoping:
            command = f"sudo -u {self.job.account} ".split()
        logging.debug(f'cleanup_on_failure command: {command}')
        scancel_out = self.executor.cancel(f'{ModeBase.get_jobid_from_file(self.slurm_jobid_file)}')
        logging.debug(f'cleanup_on_failure result: {scancel_out}')

    @abstractmethod
    def cleanup(self):
        if self.job.allow_failure:
            return
        cc_tmp = os.path.split(self.job.concurrent_tmp)[0]
        do_cleanup = 0
        for cc_id in os.listdir(cc_tmp):
            pipeline_path = f'{cc_tmp}/{cc_id}/{self.job.pipeline_id}/stages/{get_cenv("CI_JOB_STAGE")}'
            if not os.path.isdir(pipeline_path):
                continue
            for error_file in os.listdir(pipeline_path):
                if not error_file.endswith('.json'):
                    continue

                lock = FileLock(f'{pipeline_path}/{error_file}.lock')
                with lock:
                    with open(f'{pipeline_path}/{error_file}', 'r+') as error_file_fd:
                        error_file_fd.seek(0)
                        error_codes = json.load(error_file_fd)
                        for jobid in error_codes:
                            if error_codes[jobid] == -1:
                                # do_cleanup = -1
                                return
                            elif error_codes[jobid] == 1:
                                do_cleanup = 1

        if do_cleanup == 1 and self.job.shared_tmp and os.path.isdir(self.job.shared_tmp):
            logging.debug(f'doing cleanup shared_tmp={self.job.shared_tmp} command=--name=CI_{self.job.pipeline_id}')
            scancel_out = self.executor.cancel(f'--name=CI_{self.job.pipeline_id}')
            logging.debug(f'doing cleanup res={scancel_out}')
            os.system(f'rm -r {self.job.shared_tmp}')
        man.remove_id_mapping(f"{self.job.runner_path}/SlurmIDMapping.json", get_cenv("CI_JOB_ID"))
        try:
            os.rmdir(f'{self.job.runner_path}/scripts/{self.job.pipeline_id}')
        except (FileNotFoundError, OSError):
            pass

    def abort(self, error_str, exit_code=1):
        if self.job.error_code_file:
            utility.update_json_kv(self.job.error_code_file, self.job.jobid, exit_code)
        ModeBase.cleanup(self)
        logging.debug(f'Aborting with error: {error_str}')
        exit(exit_code)
