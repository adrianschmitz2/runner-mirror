concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Cluster-Runner"
  url = "https://git-ce.rwth-aachen.de/"
  token = "6KgR_1MYpDWss9644Zjz"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]

[[runners]]
  name = "CustomCluster"
  url = "https://git-ce.rwth-aachen.de/"
  token = "SHsWUE_HqX6gFENY64xd"
  executor = "custom"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.custom]
    config_exec = "/home/as388453/Runner/main.sh"
    config_args = ["config"]
    prepare_exec = "/home/as388453/Runner/main.sh"
    prepare_args = ["prepare"]
    run_exec = "/home/as388453/Runner/main.sh"
    run_args = ["run"]
    cleanup_exec = "/home/as388453/Runner/main.sh"
    cleanup_args = ["cleanup"]

