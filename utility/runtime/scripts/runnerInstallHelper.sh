#!/usr/bin/zsh

source $HOME/.zshrc

hash gitlab-runner 2>/dev/null || {

cd $HOME

curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"

rpm2cpio ./gitlab-runner_amd64.rpm | cpio -idmv

chmod +x usr/bin/gitlab-runner

# shellcheck disable=SC2016
echo 'export PATH=$PATH:$HOME/usr/bin' >> $CUSTOM_SHELL_CONFIG

}