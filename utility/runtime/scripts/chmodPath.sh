#!/usr/bin/env zsh
_path=$1
set -e
while test "$_path" != "/work"; do
    chmod o+rx "$_path"
    _path=$(dirname "$_path")
done

