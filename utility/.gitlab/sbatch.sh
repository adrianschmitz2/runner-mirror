#!/usr/bin/env bash

#SBATCH --output=$HOME/out.txt

### Request time and memory
#SBATCH --time=00:02:00

### Set Queue
#SBATCH --nodes=1
#SBATCH --ntasks=1

module list
module load Python

for i in $(seq 60);
do
    sleep 1
    echo $i
done
