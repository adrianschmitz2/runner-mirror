#!/usr/bin/env zsh

### Job name
#SBATCH --job-name=CI_TEST
###SBATCH --reservation=<advanced-reservation-id>

### File / path where STDOUT will be written, the %j is the job id
###SBATCH --output=$PWD/out.txt

### Request time and memory
#SBATCH --time=00:05:00
#SBATCH --mem-per-cpu=1500

### Set Queue
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2

module list
module load Python

echo 'Hello World'
