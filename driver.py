import sys

from core.job import Job


if len(sys.argv) < 2:
    print("Error: no argument")
    sys.exit(1)
myJob = Job(sys.argv)
