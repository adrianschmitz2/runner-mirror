#!/usr/bin/env python3
import argparse
import os
import sys
import subprocess

import core.utility.cli as cli


# Define Commandline interface
def CLI():
    subcommands = {
        'install': {
            'aliases': ['i'],
            'f': install
        },
        'remove': {
            'aliases': ['rm'],
            'f': uninstall
        },
        'update': {
            'aliases': ['u'],
            'f': update
        },
        'register': {
            'aliases': ['r'],
            'f': add_registration
        },
    }
    parameters = {
    'install': [
        {
            "flags": ['-se', '--static-exe'],
            "action": "store_true",
            "help": "Make the custom executor a static executable."
        },
        {
            "flags": ['-scc', '--static-contain-config'],
            "action": "store_true",
            "help": "Include the config inside the static executable"
        },
        {
            "flags": ["-mp", "--mapping-path"],
            "type": str,
            "metavar": "<name>",
            "default": "Assignments.txt",
            "help": "The path where the user mapping is stored."
        },
        {
            "flags": ["-d", "--downscope"],
            "action": "store_true",
            "help": "Activate the downscoping mechanism."
        },
        {
            "flags": ["-sf", "--shell-file"],
            "type": str,
            "metavar": "<name>",
            "default": ".zshrc",
            "help": "The configuration file of your systems default shell."
        },
        {
            "flags": ["-aes", "--aes-key-path"],
            "type": str,
            "metavar": "<name>",
            "default": "Keyfile",
            "help": "The path to the aes key file."
        },
        {
            "flags": ["-s", "--shell-installation"],
            "type": str,
            "metavar": "<name>",
            "default": "/usr/bin/env bash",
            "help": "The path to the preferred shell to be used."
        },
        {
            "flags": ["-kp", "--key-path"],
            "type": str,
            "metavar": "<name>",
            "default": "id_rsa",
            "help": "The path where the key for decrypting the mapping is stored."
        },
        {
            "flags": ["-lp", "--log-path"],
            "type": str,
            "metavar": "<name>",
            "default": 'logs',
            "help": "Path to the directory used to store logs."
        },
        {
            "flags": ["-pkp", "--public-key-path"],
            "type": str,
            "metavar": "<name>",
            "default": "id_rsa.pub",
            "help": "The path where the key for encrypting the mapping is stored."
        },
        {
            "flags": ["-rup", "--rt-utility-path"],
            "type": str,
            "metavar": "<name>",
            "default": "utility/runtime",
            "help": "Path where utility files are located. Downscoped users need read access."
        },
        {
            "flags": ["-up", "--user-path"],
            "type": str,
            "metavar": "<name>",
            "default": "/home/$USER/.cache/ci_driver",
            "help": "The path where the user data is stored. Only $USER is expanded if downscoping is enabled. Downscoped user needs rw access."
        },
    ],
    'remove': [],
    'update': [],
    'register': [
        {
            "flags": ['-t', '--registration-token'],
            "type": str,
            "metavar": "<token>",
            "help": "The registration token provided by the Gitlab Instance."
        },
        {
            "flags": ["-url", "--registration-url"],
            "type": str,
            "metavar": "<url>",
            "help": "Url to the used Gitlab Instance."
        },
        {
            "flags": ["-n", "--runner-name"],
            "type": str,
            "metavar": "<name>",
            "default": "Cluster-Runner",
            "help": "The name of the Gitlab Runner."
        },
        {
            "flags": ["-tag", "--tag-list"],
            "type": str,
            "metavar": "<tags>",
            "default": "cluster",
            "help": "A list of tags for the GitLab Runner."
        },
        {   "flags": ["-cc", "--concurrency"],
            "type": int,
            "metavar": "<name>",
            "default": "100",
            "help": "Maximum number of jobs the runner will execute concurrently."
         },
    ],
    'global': [
        {
            "flags": ["-ip", "--install-path"],
            "type": str,
            "metavar": "<name>",
            "default": "./install",
            "help": "The path where the runner is installed."
        },
        {
            "flags": ["-rip", "--runner-install-path"],
            "type": str,
            "metavar": "<name>",
            "default": "utility/runtime/bin",
            "help": "The path where the runner is installed."
        },
    ]
    }

    parser = argparse.ArgumentParser(prog='Aixcellenz CI Driver Installer')
    cli.add_parameters(parser, parameters['global'])
    subparsers = parser.add_subparsers(dest='cmd_name', help='sub-command help')

    for cmd in subcommands:
        subcmd_parser = subparsers.add_parser(cmd, help=f'{cmd} help', aliases=subcommands[cmd]['aliases'])
        subcmd_parser.set_defaults(func=subcommands[cmd]['f'])
        cli.add_parameters(subcmd_parser, parameters[cmd])

    ret = parser.parse_args()
    ret.install_path = os.path.abspath(os.path.expandvars(os.path.expanduser(ret.install_path)))

    # NOTE: Do *not* include the user path here. It is expanded at runtime.
    args_to_expand = ['runner_install_path', 'shell_installation', 'mapping_path',
                      'public_key_path', 'key_path', 'log_path', 'rt_utility_path']
    for arg in args_to_expand:
        try:
            vars(ret)[arg] = os.path.expandvars(os.path.expanduser(vars(ret)[arg]))
            if not os.path.isabs(vars(ret)[arg]):
                vars(ret)[arg] = os.path.join(ret.install_path, vars(ret)[arg])
        except KeyError as _:
            pass

    if ret.cmd_name == 'install':
        ret.shell_file = os.path.expandvars(os.path.expanduser(ret.shell_file))
        if not os.path.isabs(ret.shell_file):
            ret.shell_file = os.path.join(ret.rt_utility_path, ret.shell_file)

    return ret


# create initial keys
def create_keys(priv_key_path, pub_key_path):
    import rsa
    (pubkey, private_key) = rsa.newkeys(2048)
    with open(f"{pub_key_path}", "w") as text_file:
        text_file.write(pubkey.save_pkcs1().decode('ascii'))
    with open(f"{priv_key_path}", "w") as text_file:
        text_file.write(private_key.save_pkcs1().decode('ascii'))


# create initial mapping
def create_mapping(priv_key_path, pub_key_path):
    create_keys(priv_key_path, pub_key_path)
    with open(args.mapping_path, "w") as text_file:
        text_file.write('')


# Perform the initial installation of the Gitlab runner
def install():
    os.system("pip install -r requirements.txt")

    os.makedirs(args.rt_utility_path, exist_ok=True)
    # In case we don't perform an in-source installation
    # TODO: Install compiled python files
    if os.path.dirname(os.path.abspath(sys.argv[0])) != args.install_path:
        os.makedirs(args.install_path, exist_ok=True)
        if not args.static_exe:
            os.system(f'cp -r core driver.py {args.install_path}')
        os.system(f'cp -r settings AccountManager.py {args.install_path}')
        print(f'Installing utility scripts to {args.rt_utility_path}')
        os.system(f'cp -r utility/runtime/scripts {args.rt_utility_path}')
        #os.chdir(args.install_path)

    if args.static_exe:
        create_static_exe()

    _runner_install_path = args.runner_install_path
    from shutil import which
    if which("gitlab-runner") is None:
        os.makedirs(args.runner_install_path, exist_ok=True)
        os.system(f'curl -L --output {args.runner_install_path}/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"')
        os.system(f"chmod +x {args.runner_install_path}/gitlab-runner")
    else:
        _runner_install_path = os.path.dirname(os.path.abspath(which("gitlab-runner")))

    # Let the user rather do that with another invocation of the installer
    #add_registration(_runner_install_path)

    config_dir = f'{args.install_path}/settings'
    if args.static_exe and not args.static_contain_config:
        config_dir = os.getenv('XDG_CONFIG_HOME', f"{os.getenv('HOME')}/.config") + '/ci_driver'
        os.makedirs(config_dir, exist_ok=True)
    generate_driver_config(config_dir)
    generate_runner_main(args.static_exe)

    # create mapping for downscoping
    if args.downscope:
        create_mapping(args.key_path, args.public_key_path)

    # enable the gitlab-runner globally, necessary for artifacts
    # we need to do this regardless of whether the gitlab-runner is in the current PATH
    with open(args.shell_file, "a") as shell_fp:
        shell_fp.writelines(f"export PATH=$PATH:{_runner_install_path}\n")

    if args.downscope:
        set_utility_permissions(_runner_install_path, ['gitlab-runner'])
        set_utility_permissions(args.rt_utility_path)

def runner_registration(runner_path=''):
    # Register Gitlab Runner
    if runner_path: runner_path += '/'
    os.system(
        f'{runner_path}gitlab-runner register --non-interactive --url "{args.registration_url}" --registration-token "{args.registration_token}"\
                    --executor "custom" --description "{args.runner_name}" --tag-list "{args.tag_list}" --run-untagged="false" --locked="false" \
                    --access-level="not_protected"\
                    --custom-prepare-exec="{args.install_path}/main.sh" --custom-config-exec="{args.install_path}/main.sh"\
                    --custom-run-exec="{args.install_path}/main.sh" --custom-cleanup-exec="{args.install_path}/main.sh"\
                    --custom-prepare-args="prepare" --custom-config-args="config"\
                    --custom-run-args="run" --custom-cleanup-args="cleanup"')
def add_registration(runner_path=''):
    from shutil import which
    if which("gitlab-runner") is None:
        runner_registration(args.runner_install_path)
    else:
        runner_registration(runner_path)

    os.system(f"sed -i 's/concurrent = 1/concurrent = {args.concurrency}/g' $HOME/.gitlab-runner/config.toml")


# Uninstall the Gitlab runner and remove all associated processes
def uninstall():
    os.system(f"rm -rf {args.install_path}")
    #TODO: remove all potential absolute paths as well


# Update the GitLab runner
def update():
    # TODO: Should be optional
#    os.system(f'curl -L --output {args.runner_install_path}/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"')
#    os.system(f"chmod +x {args.runner_install_path}/gitlab-runner")

    # Download driver
    os.system(
        'curl -LJO "https://git-ce.rwth-aachen.de/adrianschmitz2/runner-mirror/-/jobs/artifacts/main/file/'
        'Runner.zip?job=deploy-local"')
    os.system('unzip Runner.zip')


# Generate the main path depending on the installation path
def generate_runner_main(is_exe_static=False):
    execution_line =  f'{args.install_path}/static_driver.exe' if is_exe_static else f'python3 {args.install_path}/driver.py'
    with open(f"{args.install_path}/main.sh", "w") as fp:
        fp.writelines(["#!/usr/bin/bash\n",
                       "#module load python > /dev/null 2>&1\n",
                       f"{execution_line} $@\n"])
    os.system(f"chmod +x {args.install_path}/main.sh")


def set_utility_permissions(base_path, executables=None):
    currentpath = ''
    for path in base_path.split('/'):
        currentpath += path + '/'
        subprocess.run(['chmod', 'o+rx', currentpath], env=os.environ, capture_output=True)

    if executables:
        for executable in executables:
            os.system(f'chmod o+rx {os.path.join(base_path, executable)}')
    else:
        for f in os.listdir(base_path):
            if os.path.isfile(f):
                os.system(f'chmod o+rx {f}')


# Generate the config file for the custom driver
def generate_driver_config(dest_path):
    config_json = {}

    # Define whether downscoping shall be used or not, must be the first parameter.
    config_json["Downscope"] = "True" if args.downscope else "False"

    config_json["Runner Path"]            = args.install_path
    config_json["Key Path"]               = args.key_path
    config_json["Map Path"]               = args.mapping_path
    config_json["AES Path"]               = args.aes_key_path
    config_json["Log Path"]               = args.log_path
    config_json["Shell Config"]           = args.shell_file
    config_json["User Path"]              = args.user_path
    config_json["Shell Install"]          = args.shell_installation
    config_json["Runtime Utility Path"]   = args.rt_utility_path

    with open(f'{dest_path}/config.json', "w") as fp_config:
        import json
        json.dump(config_json, fp_config, indent=4)


def create_static_exe():
    print('Creating static executable')
    os.system('pip install -r utility/install/requirements-static.txt')
    env = os.environ
    if args.static_contain_config:
        env.update({'STATIC_ADD_DATA': f'--add-data \"{args.install_path}/settings:settings\"'})
    res = subprocess.run(['./utility/install/build_static_driver.sh', f'{args.install_path}/static_driver.exe'], env=env, capture_output=True, text=True)  
    print(res.stdout)
    print(res.stderr)
    


if __name__ == '__main__':
    global args
    args = CLI()
    args.func()
